package pgpi_arkanoid;

public abstract class GameObject {
	
	/* 
	Posiciones X en las que se sitúan los dos lados verticales 
	del rectángulo que cubre el objeto:
	*/
	abstract double left();

	abstract double right();

	/* 
	Posiciones Y en las que se sitúan los dos lados horizontales 
	del rectángulo que cubre el objeto:
	*/
	abstract double top();

	abstract double bottom();
}
