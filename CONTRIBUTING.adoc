Las condiciones sobre las cuales se permitirán los diferentes cambios a introducir en el proyecto son los siguientes:

  . Los commits deberán versar sobre grupos de cambios temáticos. Es decir, se rechazaran commits que introduzcan cambios que no estén
    relacionados entre sí y/o no se enmarquen en el mismo contexto.
  . Los diferentes conjuntos de cambios deberán subirse en ramas diferentes para evitar el incumplimiento de la regla anterior.
  . Los autores no podrán subir contenido a la rama "master". Queda esta rama reservada al administrador del proyecto.
  . No se admitirá código fuente que no sea integrable con el resto del proyecto, no cumpla el funcionamiento requerido para el mismo o 
    que directamente tenga errores en tiempo de compilación o ejecución.
  . Están prohibidos cambios ajenos a lo indicado en los issues asignados al autor correspondiente. Los cambios deberán estar en el marco de 	 un issue en todo momento.
  . El contenido no deberá redactarse fuera de los límites de la profesionalidad. No se admitirá texto que contenga lenguaje soez, faltas
    de respeto o humor fuera de lugar.
