package pgpi_arkanoid;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;

public class Arkanoid extends JFrame implements KeyListener {

	/* GAME VARIABLES */

	private boolean tryAgain = false;   //Variable para controlar si se permite seguir intentandolo
	private boolean running = false;    //Varibale que indice si se esta ejecutando el juego
    private boolean pause = false;      //Variable que indica si el juego esta en pausa o no

	private Paddle paddle = new Paddle(Parametros.SCREEN_WIDTH / 2, Parametros.SCREEN_HEIGHT - 50); //crea paleta
	private Ball ball = new Ball(Parametros.SCREEN_WIDTH / 2, Parametros.SCREEN_HEIGHT / 2); //crea bola
	private List<Brick> bricks = new ArrayList<Brick>(); //crea lista de ladrillos
	private ScoreBoard scoreboard = new ScoreBoard();    //crea tablero de puntuacion

	private double lastFt;          //último fotograma, guarda el tiempo en que se ejecuta 
	private double currentSlice;    //acumulador de tiempo de fotogramas

    //Metodo para detectar interseccion entre dos objetos, devuelve true si la hay
	boolean isIntersecting(GameObject mA, GameObject mB) {
        /* Los objetos tienen unas coordenadas que determinan su centro. Las funciones left, right, top y 
           bottom determinan el area ocupada por dichos objetos. Comparando estos 4 valores de los dos objetos
           podemos ver si se esta produciendo una interseccion */
		return mA.right() >= mB.left() && mA.left() <= mB.right() 
				&& mA.bottom() >= mB.top() && mA.top() <= mB.bottom();
	}

    //Metodo para detectar colisiones del paddle con la bola
	void testCollision(Paddle mPaddle, Ball mBall) {
		if (!isIntersecting(mPaddle, mBall)) //si hay interseccion sale
			return;
		mBall.velocityY = -Math.abs(mBall.velocityY);    //cambio el sentido de la bola en el eje Y
		if (mBall.x < mPaddle.x)                        //si esta la boca a la izquierda de la paleta
			mBall.velocityX = -Math.abs(mBall.velocityX);    //cambia el sentido de la bola en el eje X
		else
			mBall.velocityX = Math.abs(mBall.velocityX); //no cambia en sentido en el eje X
	}

    //Metodo para detectar colisiones de la bola con los ladrillos
	void testCollision(Brick mBrick, Ball mBall, ScoreBoard scoreboard) {

		if (!isIntersecting(mBrick, mBall)) 
			return;
		
		// Si el ladrillo no aguanta mas choques, lo destruimos, aumentamos la velocidad y sumamos un punto:
		if(mBrick.choquesRestantes <= 0) {
			mBrick.destroyed = true;
			if(Math.abs(mBall.velocityX) + Parametros.BALL_SPEED_STEP < Parametros.BALL_MAX_SPEED) {
				mBall.velocityX = mBall.velocityX > 0 ?  mBall.velocityX + Parametros.BALL_SPEED_STEP : mBall.velocityX - Parametros.BALL_SPEED_STEP;
				mBall.velocityY = mBall.velocityY > 0 ?  mBall.velocityY + Parametros.BALL_SPEED_STEP : mBall.velocityY - Parametros.BALL_SPEED_STEP;
			}
			scoreboard.increaseScore();
		}
		// El ladrillo ahora aguantara un choque menos:
		mBrick.choquesRestantes--;

        //calcula superposiciones segun los tamaños de los objetos y sus posiciones:
		double overlapLeft = mBall.right() - mBrick.left();     //bola a la izquierda, ladrillo a la derecha
		double overlapRight = mBrick.right() - mBall.left();    //ladrillo a la izquierda, bola a la derecha
		double overlapTop = mBall.bottom() - mBrick.top();      //bola arriba, ladrillo debajo
		double overlapBottom = mBrick.bottom() - mBall.top();   //ladrillo arriba, bola debajo

		boolean ballFromLeft = overlapLeft < overlapRight; //calcula si la bola viene de la izquierda
		boolean ballFromTop = overlapTop < overlapBottom;  //calcula si la bola viene de arriba

        /* con los valores anteriores calcula valores de superposicion de X,Y segun venga de la izquierda o derecha
           y de abajo o de arriba */ 
		double minOverlapX = ballFromLeft ? overlapLeft : overlapRight; 
		double minOverlapY = ballFromTop ? overlapTop : overlapBottom;

        /* recalcula las velocidades en ambos ejes en un sentido u otro dependiendo de si choca a la izquierda, derecha
            arriba o abajo */
		if (minOverlapX < minOverlapY) {
			mBall.velocityX = ballFromLeft ? -Math.abs(mBall.velocityX) : Math.abs(mBall.velocityX);
		} else {
			mBall.velocityY = ballFromTop ? -Math.abs(mBall.velocityY) : Math.abs(mBall.velocityY);
		}
	}
    
    //Metodo de inicilizacien de los ladrillos
	void initializeBricks(List<Brick> bricks) {
		// deallocate old bricks
		bricks.clear();

		for (int iX = 0; iX < Parametros.COUNT_BLOCKS_X; ++iX) {
			for (int iY = 0; iY < Parametros.COUNT_BLOCKS_Y; ++iY) {
				bricks.add(new Brick((iX + 1) * (Parametros.BLOCK_WIDTH + 3) + 22,
						(iY + 2) * (Parametros.BLOCK_HEIGHT + 3) + 20));
			}
		}
	}

    //Metodo constructor de la clase Arkanoid
	public Arkanoid() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//Establecer operacion por defecto al cerrar
		this.setUndecorated(false);                         //Ocultar o no barra de de título
		this.setResizable(false);                           //Permitir o no cambiar el tamanio de la ventana
		this.setSize(Parametros.SCREEN_WIDTH, Parametros.SCREEN_HEIGHT); //Establecer anchura y altura de la ventana
		this.setVisible(true);                              //Establecer la ventana como visible
		this.addKeyListener(this);                          //Añade el listener
		this.setLocationRelativeTo(null);                   //Establece localizacion relativa

		this.createBufferStrategy(2);                       //Crea doble buffer de memoria para dibujar

		initializeBricks(bricks); //Se inicializan los ladrillos

	}

	//Esta funcion sera la primera que se ejecute en el codigo, iniciara nuestro juego
	void run() {
		Languages.languageInitialization();
		//Pintar el fondo de negro
		BufferStrategy bf = this.getBufferStrategy();
		Graphics g = bf.getDrawGraphics();
		g.setColor(Color.black);
		g.fillRect(0, 0, getWidth(), getHeight());
		//Se guarda el estado actual del juego: en ejecucion
		running = true;
        pause = false; //inicialmente el juego no esta en pausa

		// Llama a la funcion de generar pantalla de titulo
        pantallaPrincipal();
		while (running) {
            while (!pause){
			    long time1 = System.currentTimeMillis();
			    //Si el juego se encuentra en proceso de ejecucion entra a la funcion de actualizacion y tras la evaluacion de los nuevos 
			    //valores actualiza las imagenes en pantalla con drawScene. Utilizamos la funcion sleep para reducir la tasa de fotogramas.
			    if (!scoreboard.gameOver && !scoreboard.win) {
				    tryAgain = false;
				    update();
				    drawScene(ball, bricks, scoreboard);

				    // Para reducir la tasa de fotogramas
				    try {
					    Thread.sleep(10);
				    } catch (InterruptedException e) {
					    e.printStackTrace();
				    }
			    //Si por el contrario se quiere reiniciar el juego se reinician las estadísticas y los objetos vuelven a la posicion inicial, volviendo a la pantalla de inicio
			    } else {
				    if (tryAgain) {
					    tryAgain = false;
					    initializeBricks(bricks);
					    scoreboard.lives = Parametros.PLAYER_LIVES;
					    scoreboard.score = 0;
					    scoreboard.win = false;
					    scoreboard.gameOver = false;
					    scoreboard.updateScoreboard();
					    ball.x = Parametros.SCREEN_WIDTH / 2;
					    ball.y = Parametros.SCREEN_HEIGHT / 2;
					    ball.velocityX = Parametros.BALL_VELOCITY; 
					    ball.velocityY = Parametros.BALL_VELOCITY;
					    paddle.x = Parametros.SCREEN_WIDTH / 2;
				    }
			    }
			    //Calcula la tasa de fotogramas en base a el tiempo transcurrido entre la ultima actualizacion y este instante.
     			//Se muestra en el titulo de la ventana
			    long time2 = System.currentTimeMillis();
			    double elapsedTime = time2 - time1;

			    lastFt = elapsedTime;

			    double seconds = elapsedTime / 1000.0;
			    if (seconds > 0.0) {
				    double fps = 1.0 / seconds;
				    this.setTitle("FPS: " + fps);
			    }
                
                //por si se pulsa escape mientras se esta en pausa
                if (!running)
                    this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
            }

            //Para el control de la pausa, esperamos para no bloquear el programa            
			try {
				Thread.sleep(10);
                
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//Cierra la ventana
		this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}

	private void update() {
		//Lleva el recuento del tiempo transcurrido para actualizar a una cadencia constante
		currentSlice += lastFt;
		//Actualiza la pelota, la raqueta y comprueba las colisiones para destruir un ladrillo
		for (; currentSlice >= Parametros.FT_SLICE; currentSlice -= Parametros.FT_SLICE) {
			
			ball.update(scoreboard, paddle);
			paddle.update();
			testCollision(paddle, ball);

			Iterator<Brick> it = bricks.iterator();
			while (it.hasNext()) {
				Brick brick = it.next();
				testCollision(brick, ball, scoreboard);
				if (brick.destroyed) {
					it.remove();
				}
			}

		}
	}
	
	// Función de generación de la pantalla principal
	private void pantallaPrincipal(){
		// Mientras no se pulse enter...
		while(!tryAgain){
			// Indicamos el texto
			scoreboard.customText(Languages.lang.get("Welcome"));
			// Creamos el buffer de dibujo
			BufferStrategy bf = this.getBufferStrategy();
			Graphics g = null;
			//Dibuja el fondo negro y el texto
			try {
				g = bf.getDrawGraphics();
				g.setColor(Color.black);
				g.fillRect(0, 0, getWidth(), getHeight());

				// Indicamos que hemos ganado para internamente usar un tipo de fuente específica
				scoreboard.win = true;
				scoreboard.draw(g);
				scoreboard.win = false;
				
			} finally {
				g.dispose();
			}
			
			// Lo mostramos
			bf.show();

			Toolkit.getDefaultToolkit().sync();
			// Esperamos para no bloquear al programa
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
	}
	
	//Codigo para el dibujado de la escena
	private void drawScene(Ball ball, List<Brick> bricks, ScoreBoard scoreboard) {
		
		BufferStrategy bf = this.getBufferStrategy();
		Graphics g = null;
		//Dibuja el fondo negro, la pelota, la raqueta, los ladrillos y la puntuacion.
		try {

			g = bf.getDrawGraphics();

			g.setColor(Color.black);
			g.fillRect(0, 0, getWidth(), getHeight());

			ball.draw(g);
			paddle.draw(g);
			for (Brick brick : bricks) {
				brick.draw(g);
			}
			scoreboard.draw(g);

		} finally {
			g.dispose();
		}

		bf.show();

		Toolkit.getDefaultToolkit().sync();

	}

    //Metodo que actuliza la variable que controla la pausa
    private void pause(){
        if (pause){   //se pulsa P por segunda vez para volver al juego
            pause = false;
            scoreboard.updateScoreboard(); //actualiza de nuevo con la puntuacion y las vidas actuales
            drawScene(ball, bricks, scoreboard);
        }
        else{   //se pulsa P
            pause = true;
            scoreboard.text="PAUSE"; //pone el texto "PAUSE" en el scoreboard
            drawScene(ball, bricks, scoreboard);
        }
    }

	//Funcion para la comprobacion de las teclas que son pulsadas
	@Override
	public void keyPressed(KeyEvent event) {
		//Si se pulsa escape para la ejecucion poniendo la variable running a false.
		if (event.getKeyCode() == KeyEvent.VK_ESCAPE) {
			running = false;
		}
		//Si se pulsa enter se reinicia el juego activando la variable tryAgain
		if (event.getKeyCode() == KeyEvent.VK_ENTER) {
			tryAgain = true;
		}
        //Si se pulsa P se pone el juego en pausa
        if (event.getKeyCode() == KeyEvent.VK_P) {
            pause();
		}
		//Inicia el movimiento a izquierda o derecha segun pulsemos las flechas izquierda o derecha
		switch (event.getKeyCode()) {
		case KeyEvent.VK_LEFT:
			paddle.moveLeft();
			break;
		case KeyEvent.VK_RIGHT:
			paddle.moveRight();
			break;
		default:
			break;
		}
	}

	//Detiene el movimiento iniciado por la funcion keyPressed
	@Override
	public void keyReleased(KeyEvent event) {
		switch (event.getKeyCode()) {
		case KeyEvent.VK_LEFT:
		case KeyEvent.VK_RIGHT:
			paddle.stopMove();
			break;
		default:
			break;
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {

	}
	//Funcion main que arranca la ejecucion a través del metodo run
	public static void main(String[] args) {
		new Arkanoid().run();
	}

}
