package pgpi_arkanoid;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

//Clase que define los ladrillos (que la bola destruye)
public class Brick extends Rectangle {

	//Propiedad que indica si ha sido destruido o no (False por defecto)
	boolean destroyed = false;
	
	// Estado del ladrillo, o choques de pelota que quedan para que el ladrillo se rompa:
	int choquesRestantes = 0;
	// C�digo de colores de ladrillo, de peor a mejor estado: 
	Color[] colores = {Color.RED, Color.ORANGE, Color.YELLOW};

	//Constructor de la clase (Recibe la posición del ladrillo)
	Brick(double x, double y) {
		//Posición del ladrillo
		this.x = x;
		this.y = y;
		//Tamaño del ladrillo
		this.sizeX = Parametros.BLOCK_WIDTH;
		this.sizeY = Parametros.BLOCK_HEIGHT;
		
		// Inicializamos el estado del ladrillo de manera aleatoria:
		this.choquesRestantes = (new Random()).nextInt(3);
	}

	//Función que dibuja el ladrillo
	void draw(Graphics g) {

		// Coloreamos el ladrillo con el color que corresponde con su estado:
		g.setColor(colores[choquesRestantes]);
		
		g.fillRect((int) left(), (int) top(), (int) sizeX, (int) sizeY);
	}
}
