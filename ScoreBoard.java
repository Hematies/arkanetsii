package pgpi_arkanoid;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

public class ScoreBoard {
	
	// Variables propias de la clase
	int score = 0; // puntuación
	int lives = Parametros.PLAYER_LIVES; // Vidas restantes del jugador
	boolean win = false; // La condición de si ha ganado 
	boolean gameOver = false; // la condición de si ha perdido
	String text = ""; // Texto a representar en el cuadro de juego

	Font font;
	
	// Inicialización de la clase
	ScoreBoard() {
		font = new Font(Parametros.FONT, Font.PLAIN, 12);
		text = Languages.lang.get("ScoreWelcome");
	}

	// Función que cada vez que se rompa un ladrillo, se encarga de incrementar la puntuación y comprobar si se ha ganado, es decir, rotos todos
	void increaseScore() {
		score++;
		// Si se han roto todos los ladrillos, se monta el mensaje de victoria
		if (score == (Parametros.COUNT_BLOCKS_X * Parametros.COUNT_BLOCKS_Y)) {
			win = true;
			text = Languages.lang.get("WinText") + score
					+ Languages.lang.get("PressRestart");
		} else { // En caso contrario, se actualiza la barra de puntuación actual
			updateScoreboard();
		}
	}

	// Función llamda en caso de perder la partida
	void die() {
		// Se pierde una vida
		lives--;
		// En caso de no quedar más vida se muestra el mensaje de game over
		if (lives == 0) {
			gameOver = true;
			text = Languages.lang.get("LoseText") + score
					+ Languages.lang.get("PressRestart");
		} else { // En caso contrario, se actualiza la barra de `puntaciones con una vida menos
			updateScoreboard();
		}
	}
	
	// Función que permite insertar un texto personalizado
	void customText(String text){
		this.text = text;
	}

	// Actualiza la barra de puntuación y vida con los valores que presente este actualmente
	void updateScoreboard() {
		text = Languages.lang.get("Score") + score + Languages.lang.get("Lives") + lives;
	}

	// Función principal de la clase, se encarga de pintar sobre el lienzo g, el texto
	void draw(Graphics g) {
		// Elección del estilo de texto según si se sigue el juego o se ha perdido/ganado
		if (win || gameOver) { // En el segundo caso
			font = font.deriveFont(35f); // Fuente de tamaño 35
			FontMetrics fontMetrics = g.getFontMetrics(font);
			g.setColor(Color.WHITE); // Letras de color blanco
			g.setFont(font);
			int titleHeight = fontMetrics.getHeight();
			int lineNumber = 1;
			// Se escribe el mensaje, en la parte central de la pantalla, teniendo en cuenta los saltos de líneas
			for (String line : text.split("\n")) {
				int titleLen = fontMetrics.stringWidth(line);
				g.drawString(line, (Parametros.SCREEN_WIDTH / 2) - (titleLen / 2),
						(Parametros.SCREEN_HEIGHT / 4) + (titleHeight * lineNumber));
				lineNumber++;

			}
			// En caso contrario, el juego continue...
		} else {
			font = font.deriveFont(34f); // Tamaño de fuente 34
			FontMetrics fontMetrics = g.getFontMetrics(font);
			g.setColor(Color.WHITE); // color blanco las letras
			g.setFont(font);
			int titleLen = fontMetrics.stringWidth(text);
			int titleHeight = fontMetrics.getHeight();
			// La cadena esta vez se coloca en la parte superior, para no interfereir con el juego
			g.drawString(text, (Parametros.SCREEN_WIDTH / 2) - (titleLen / 2),
					titleHeight + 15);

		}
	}

}
