package pgpi_arkanoid;

import java.util.Hashtable;
import java.util.Map;

public class Languages{
	public static Map<String, String> lang = new Hashtable<>();
	
	public static void languageInitialization() {
		switch (Parametros.LANG) {
		case "es":
			lang.put("Welcome","\n\n Bienvenido a ARKANETSII,\n\n ¡disfrute del juego! \n\n Presiona enter para empezar");
			lang.put("ScoreWelcome","Bienvenidos a la versión Java Arkanoid");
			lang.put("WinText", "¡Has ganado! \nTu puntuacion fué:");
			lang.put("LoseText", "¡Has perdido! \nTu puntuacion fué:");
			lang.put("PressRestart", "\n\nPulsa Intro para reiniciar");
			lang.put("Lives", "  Vidas: ");
			lang.put("Score", "Puntuación: ");
			break;

		case "en":
			lang.put("Welcome","\n\n Welcome to ARKANETSII,\n\n ¡enjoy the game! \n\n Press enter to start");
			lang.put("ScoreWelcome", "Welcome to Arkanoid Java version");
			lang.put("WinText", "You have won! \nYour score was: ");
			lang.put("LoseText", "You have lost! \nYour score was: ");
			lang.put("PressRestart", "\n\nPress Enter to restart");
			lang.put("Lives", "  Lives: ");
			lang.put("Score", "Score: ");
			break;
			
		default:
			lang.put("Welcome","\n\n Bienvenido a ARKANETSII,\n\n ¡disfrute del juego! \n\n Presiona enter para empezar");
			lang.put("ScoreWelcome","Bienvenidos a la versión Java Arkanoid");
			lang.put("WinText", "¡Has ganado! \nTu puntuacion fué:");
			lang.put("LoseText", "¡Has perdido! \nTu puntuacion fué:");
			lang.put("PressRestart", "\n\nPulsa Intro para reiniciar");
			lang.put("Lives", "  Vidas: ");
			lang.put("Score", "Puntuación: ");
			break;
		}	
	}
	private static final long serialVersionUID = 1L;
	
	
	
}
