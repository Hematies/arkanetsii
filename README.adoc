= ArkanEtsii
:toc:
:toclevels: 2
:toc-title: Índice

== *Descripción*

Remake del juego Arkanoid realizado por alumnos de la ETSII en Java, sobre la base del juego original, con el fin de obtener un software ordenado (siguiendo la lógica de clases de Java), comentado y con nuevas funcionalidades.

== *Objetivos*
Los objetivos a conseguir en este proyecto son:

	. Modularización del archivo princial según las clases y su relación.
	. Comentar el código para una mejor comprensión del mismo.
	. Añadir mejoras de funcionalidad al código.
	. Automatizar el despliegue de la documentación en formato html.
	. Automatizar la semicompilación del código para analizar posibles errores de implentación y así garantizar un código correcto y verificado.

== *Especificaciones*

=== *Flujo de trabajo*

El flujo de trabajo que se sigue en este proyecto está basado en el *_flujo github_* con ligeras modificaciones. Tras realizar el _fork_ del respositorio, este flujo sería el siguiente:

  . Comprobrobación y obtención de cambios de la rama *_dev_* mediante el comando _pull_ o _fetch_ sobre el respositorio local, para evitar trabajar con versiones desactualizadas.
  . Elaboración de una rama temática para cada cambio que se desee realizar.
  . Tras confirmar los cambios en la rama temática sobre repositorio local, llevarlo al repositorio remoto con el comando _push_ .
  . Sobre el repositorio remoto personal, elaborar un _merge request_ de la rama temática con los cambios desarrollados sobre la rama dev.
  . El administrador/encargado del proyecto revisará los cambios introducidos:

    * En caso de que esté todo en orden con las reglas de contribución, se aceptarán e integrarán los cambios en dev.
    * En caso de no cumplir las reglas de contribución o conflictos en el _merge_ de los cambios:
      .. El administrador no lo aceptará y comentará el motivo del mismo por medio de la interfaz de diálogo que ofrece gitlab.
      .. El colaborador debe tomar en consideración las indicaciones, corregirlas y comunicarlo por medio de la misma herramienta.
      .. Este ciclo se repetirá hasta que los cambios cumplan con todas las reglas establecidas.

  . Si los cambios producidos están asociados a un _issue_, este se cerrará.

=== *Reglas de contribución*

La lista completa de reglas se presenta en el siguiente <<CONTRIBUTING.adoc#,documento>> .

== *Autores*

https://gitlab.com/dancasmor[Casanueva Morato, Daniel]

https://gitlab.com/guillermofdez98[Fernández Caballero, Guillermo]

https://gitlab.com/mascse79[Sánchez Cano, Miguel Ángel]

https://gitlab.com/Hematies[Sánchez Cuevas, Pablo]

https://gitlab.com/Ampaex[Vázquez Pérez, Antonio]


== *Anexo*

  . Se omite en los comentarios del código el uso de la acentuación y la 'ñ' para evitar problemas de viusalización y/o compilación.
  . Se considerarán correctas las traducciones de "paleta" y "raqueta" para "paddle" y de "ladrillo" y "bloque" para "brick", respetando así la libre elección de la traducción del inglés de los desarrolladores, siendo ambas correctas en cada caso.

== *Licencia*

* *GPL*  (Licencia Pública General de GNU).


Este repositorio es documentación libre: puede redistribuirlo y / o modificarlo bajo los términos de la Licencia Pública General GNU publicada por Free Software Foundation, ya sea la versión 3 de la Licencia, o (a su elección) cualquier versión posterior.


Este programa se distribuye con la esperanza de que sea útil, pero *SIN NINGUNA GARANTÍA*; sin siquiera la garantía implícita de *COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO EN PARTICULAR*. Ver el Licencia pública general de GNU para más detalles.


Debería haber recibido una copia de la Licencia Pública General de GNU junto con este programa. Si no, vea https://www.gnu.org/licenses/[el siguiente enlace].

== *Manual de juego*

Bienvenido al juego Arkanetsii, remake del famoso videojuego arkanoid, pero ahora basado en java.

=== *Controles*
* Utiliza las flechas | <- | -> | para mover la raqueta.
* | ESC | para salir del juego.
* | INTRO | para iniciar el juego.
* Pulsa | P | en cualquier momento para detener el juego.

=== *Dinámica de juego*
Al iniciar se mostrará la pantalla de título, pulsa intro para comenzar.

El [underline]#objetivo principal# consiste en [underline]#romper todos los bloques# que se muestran en la parte superior de la pantalla, para lograrlo hay que hacer uso de la pelota y la raqueta.

Mueve la raqueta de forma que la pelota pueda rebotar sobre ella y volver hacia la zona de los bloques, así tendrás mayores posibilidades de romper uno.

[underline]#Pero recuerda:#
|===

Si la pelota escapa por el límite inferior de la pantalla perderás una vida, [underline]#cuado se te acaben las vidas tu partida habrá terminado#.

|===

Existen bloques de 3 colores distintos según el número de golpes para su desctrucción:

* [red]#Rojos#: 1 golpe restante.
* [olive]#Naranjas#: 2 golpes restantes.
* [yellow]#Amarillos#: 3 golpes restantes.

[underline]#La destrucción de un bloque implica un aumento en la velocidad de la pelota#, así consecutivamente hasta una velocidad umbral que será la dificultad máxima del juego.

Si has conseguido ganar...

==== ¡Felicidades!

== *Ficheros del proyecto*

Además de la documentación propia ( *README.adoc* y *CONTRIBUTING.adoc* ), así como el archivo de CI/CD que se explicará en el siguiente apartado, tenemos:

* *Arkanoid.java* -> clase principal del juego que contiene la incialización de todos los objetos necesarios para desarrollar el juego y aquellas funciones relacionadas con la interacción entre varios de estos objetos.
* *Parametros.java* -> clase orientada a contener todas las constantes y parámetros del programa reunidas en el mismo punto para su fácil manipulación.
* *GameObject.java* -> clase abstracta cuya función se asimila a la de una interfaz, es decir, ser base común a un conjuto de clases de objetos con movimiento en el juego.
* *Reactangle.java* -> clase base con las funciones y propiedades geométricas mínimas para poder establecer los objetos de juego en base a ella.
* *Brick.java* , *Ball.java* y *Paddle.java* -> clases derivadas de las dos anteriores. Encargadas de encapsular la representación y funcionalidad los objetos con los que se interactúa en el juego, es decir, los ladrillos a romper, la bola a golpear para romperlos y la paleta/raqueta necesaria para golpear la bola.
* *ScoreBoard.java* -> aquella clase que representa la información textual que aparece en el juego, en definitiva, la tabla de puntuación, vidas y mensajes guías.
* *Languaje.java* -> clase empleada para llevar a cabo la adaptación del juego a varios idiomas. Se reune el ella toda la funcionalidad de traducción, así como las propias traducciones.

== *Ficheros compilados del proyecto*

https://dancasmor.gitlab.io/arkanetsii/pgpi_arkanoid.zip[Enlace de descarga del fichero comprimido con el codigo compilado]
